<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Diplomado Java</title>
<link rel="stylesheet" href="../css/main.css">
<link rel="stylesheet" href="../css/form.css">
<link rel="stylesheet" href="../css/a.css">

<script src="../js/jquery-1.7.2.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/notas.js"></script>
</head>
<body>
	
<form class="clasico" action="http://localhost:8081/Semana9ApiRest/rest/autos" method="POST" style="width: 330px">
		<fieldset>
			<legend>Nuevo Auto</legend>
			   <div>
                    <label style="width:70px">Codigo</label>
                    <input type="text" name="codigo"                            
                           id="codigo" maxlength="50"
                           style="width:80px">
                </div>
                <div>
                    <label style="width:70px">Marca</label>
                    <input type="text" name="marca"                            
                           id="marca" maxlength="50"
                           style="width:220px">
                </div>
                <div>
                    <label style="width:70px">Modelo</label>
                    <input type="text" name="modelo"                            
                           id="modelo" maxlength="50"
                           style="width:220px">
                </div>
                <div>
                    <label style="width:70px">Precio</label>
                    <input type="text" name="precio"                            
                           id="precio" maxlength="50"
                           style="width:80px">
                </div>                
							
			<div class="center">
				<input type="submit" value="Enviar Datos" class="submit">
			</div>
		</fieldset>
	</form>

	<p style="text-align: center">
		<a class="clasico" href="notasQry.jsp">Cancelar</a>
	</p>
</body>
</html>
