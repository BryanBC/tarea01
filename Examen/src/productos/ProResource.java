package jaxrs.productos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

public class ProResource {
	
	@Context
	UriInfo uriInfo;	
	@Context
	Request request;
	//Identificador de la entidad
	String codigo;
	
	public ProResource(
			UriInfo uriInfo,
			Request request,
			String codigo){
		this.uriInfo = uriInfo;
		this.request = request;
		this.codigo = codigo;
	}
	
	@GET
	@Produces(MediaType.TEXT_XML)
	public Producto getProducto(){
		
		Producto producto = null;
		
		try {
			Connection cn = 
					MysqlDBConn.getConnection();
			
			PreparedStatement pst = 
					cn.prepareStatement(
							"SELECT * FROM producto "+
							"where codigo=?");
			
			
			pst.setString(1, codigo);
		
			ResultSet rs = pst.executeQuery();
			
			if(rs.next()){			
				producto = new Producto();
				producto.setCodigo(rs.getString(1));
				producto.setNombre(rs.getString(2));
				producto.setStock(rs.getString(3));
				producto.setPrecio(rs.getDouble(4));	
			}		
			cn.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}	

		return producto;
	}
}
