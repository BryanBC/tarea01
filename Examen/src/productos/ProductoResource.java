package jaxrs.productos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

//Url del servicio
// Ejm : http://host:port/rest/autos
@Path("/autos")
public class ProductoResource {
	
	@Context
	UriInfo uriInfo;
	
	@Context
	Request request;
	
	@GET
	@Produces(MediaType.TEXT_XML)
	//@Produces("application/json")
	public List<Producto> listarProductoXML(){
		
		System.out.println("Invocando " +
				"listarProductoXML");
		
		List<Producto> listaProducto = 
				new ArrayList<Producto>();
		try {
			Connection cn = 
					MysqlDBConn.getConnection();
			
			PreparedStatement pst = 
					cn.prepareStatement(
							"SELECT * FROM producto");
		
			ResultSet rs = pst.executeQuery();
			
			Producto producto = null;
			
			while(rs.next()){	
				
				producto = new Producto();
				producto.setCodigo(rs.getString(1));
				producto.setNombre(rs.getString(2));
				producto.setStock(rs.getString(3));
				producto.setPrecio(rs.getDouble(4));	
				listaProducto.add(producto);
			}
			cn.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return listaProducto;
	}
	
	@Path("{codigo}")
	public ProResource getProducto(
			@PathParam("codigo") String codigo){
		
		return new ProResource(uriInfo, 
				request, codigo);
	}

	@POST
	@Consumes(
		MediaType.APPLICATION_FORM_URLENCODED)
	public void nuevoProducto(
			@FormParam("codigo") String codigo,
			@FormParam("nombre") String nombre,
			@FormParam("stock") String stock,
			@FormParam("precio") Double precio){
		
		Producto auto = new Producto();
		auto.setCodigo(codigo);
		auto.setNombre(nombre);
		auto.setStock(stock);
		auto.setPrecio(precio);
		
		try {
			
			Connection cn = MysqlDBConn.getConnection();
			
			PreparedStatement pst =
					cn.prepareStatement(
				"INSERT INTO " +
				"producto(codigo,nombre,stock,precio) "+
				"values (?,?,?,?) ");
			
			pst.setString(1, auto.getCodigo());
			pst.setString(2, auto.getNombre());
			pst.setString(3, auto.getStock());
			pst.setDouble(4, auto.getPrecio());
			pst.executeUpdate();
			cn.close();
			System.out.println("Producto registrado " + 
						auto.getCodigo());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}	
}
